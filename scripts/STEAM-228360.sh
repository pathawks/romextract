#!/bin/sh

# Extraction script for:
# Full Throttle Remastered (full.data)

# Outputs:
# - Full Throttle (Version B/English)

romextract()
{
	if [ -f "$(dirname "$FILE")/full.data" ]; then
		FILE="$(dirname "$FILE")/full.data"
	else
		echo "Could not find full.data"
		return 1
	fi

	echo "Extracting game files from full.data ..."
	mkdir -p "$SCRIPTID/DATA" "$SCRIPTID/VIDEO"
	# tail for offset, head for game file size
	tail -c +2259807909 "$FILE" | head -c +19697 \
		> "$SCRIPTID/ft.la0"
	tail -c +2259827606 "$FILE" | head -c +156031779 \
		> "$SCRIPTID/ft.la1"
	tail -c +2415859385 "$FILE" | head -c +106151300 \
		> "$SCRIPTID/monster.sou"
	tail -c +3582302324 "$FILE" | head -c +160544 \
		> "$SCRIPTID/DATA/BENBIKE.NUT"
	tail -c +3582462868 "$FILE" | head -c +6922 \
		> "$SCRIPTID/DATA/BENCUT.NUT"
	tail -c +3582469790 "$FILE" | head -c +660794 \
		> "$SCRIPTID/DATA/BENFLIP.SAN"
	tail -c +3583130584 "$FILE" | head -c +7176 \
		> "$SCRIPTID/DATA/BENSGOGG.NUT"
	tail -c +3583137760 "$FILE" | head -c +451416 \
		> "$SCRIPTID/DATA/CHASEBEN.SAN"
	tail -c +3583589176 "$FILE" | head -c +1076518 \
		> "$SCRIPTID/DATA/CHASOUT.SAN"
	tail -c +3584665694 "$FILE" | head -c +512282 \
		> "$SCRIPTID/DATA/CHASTHRU.SAN"
	tail -c +3585177976 "$FILE" | head -c +1184422 \
		> "$SCRIPTID/DATA/FISHFEAR.SAN"
	tail -c +3586362398 "$FILE" | head -c +74568 \
		> "$SCRIPTID/DATA/FISHGOG2.SAN"
	tail -c +3586436966 "$FILE" | head -c +68672 \
		> "$SCRIPTID/DATA/FISHGOGG.SAN"
	tail -c +3586505638 "$FILE" | head -c +724788 \
		> "$SCRIPTID/DATA/GETNITRO.SAN"
	tail -c +3587230426 "$FILE" | head -c +768 \
		> "$SCRIPTID/DATA/GOGLPALT.RIP"
	tail -c +3587231194 "$FILE" | head -c +743166 \
		> "$SCRIPTID/DATA/HITDUST1.SAN"
	tail -c +3587974360 "$FILE" | head -c +408008 \
		> "$SCRIPTID/DATA/HITDUST2.SAN"
	tail -c +3588382368 "$FILE" | head -c +176670 \
		> "$SCRIPTID/DATA/HITDUST3.SAN"
	tail -c +3588559038 "$FILE" | head -c +201492 \
		> "$SCRIPTID/DATA/HITDUST4.SAN"
	tail -c +3588773032 "$FILE" | head -c +4492 \
		> "$SCRIPTID/DATA/ICONS2.NUT"
	tail -c +3588760530 "$FILE" | head -c +12502 \
		> "$SCRIPTID/DATA/ICONS.NUT"
	tail -c +3588777524 "$FILE" | head -c +194746 \
		> "$SCRIPTID/DATA/LIFTBORD.SAN"
	tail -c +3588972270 "$FILE" | head -c +269924 \
		> "$SCRIPTID/DATA/LIFTCHAY.SAN"
	tail -c +3589242194 "$FILE" | head -c +258958 \
		> "$SCRIPTID/DATA/LIFTGOG.SAN"
	tail -c +3589501152 "$FILE" | head -c +424560 \
		> "$SCRIPTID/DATA/LIFTMACE.SAN"
	tail -c +3589925712 "$FILE" | head -c +237490 \
		> "$SCRIPTID/DATA/LIFTSAW.SAN"
	tail -c +3590163202 "$FILE" | head -c +869026 \
		> "$SCRIPTID/DATA/LIMOCRSH.SAN"
	tail -c +3591032228 "$FILE" | head -c +6004 \
		> "$SCRIPTID/DATA/MINEDRIV.FLU"
	tail -c +3591038232 "$FILE" | head -c +84362390 \
		> "$SCRIPTID/DATA/MINEDRIV.SAN"
	tail -c +3675400622 "$FILE" | head -c +329030 \
		> "$SCRIPTID/DATA/MINEEXIT.SAN"
	tail -c +3675729652 "$FILE" | head -c +6004 \
		> "$SCRIPTID/DATA/MINEFITE.FLU"
	tail -c +3675735656 "$FILE" | head -c +83250252 \
		> "$SCRIPTID/DATA/MINEFITE.SAN"
	tail -c +3846945542 "$FILE" | head -c +9285 \
		> "$SCRIPTID/DATA/MINEROAD.TRS"
	tail -c +3758985908 "$FILE" | head -c +824 \
		> "$SCRIPTID/DATA/ROADRASH.NUT"
	tail -c +3758986732 "$FILE" | head -c +768 \
		> "$SCRIPTID/DATA/ROADRASH.RIP"
	tail -c +3758987500 "$FILE" | head -c +824 \
		> "$SCRIPTID/DATA/ROADRSH2.NUT"
	tail -c +3758988324 "$FILE" | head -c +768 \
		> "$SCRIPTID/DATA/ROADRSH2.RIP"
	tail -c +3758989092 "$FILE" | head -c +768 \
		> "$SCRIPTID/DATA/ROADRSH3.RIP"
	tail -c +3758989860 "$FILE" | head -c +3998826 \
		> "$SCRIPTID/DATA/ROTTFITE.SAN"
	tail -c +3762988686 "$FILE" | head -c +468686 \
		> "$SCRIPTID/DATA/ROTTFLIP.SAN"
	tail -c +3763457372 "$FILE" | head -c +5870718 \
		> "$SCRIPTID/DATA/ROTTOPEN.SAN"
	tail -c +3846954827 "$FILE" | head -c +29644 \
		> "$SCRIPTID/DATA/SCUMMFNT.NUT"
	tail -c +3846984471 "$FILE" | head -c +904 \
		> "$SCRIPTID/DATA/SPECFNT.NUT"
	tail -c +3846985375 "$FILE" | head -c +24754 \
		> "$SCRIPTID/DATA/TECHFNT.NUT"
	tail -c +3847010129 "$FILE" | head -c +39998 \
		> "$SCRIPTID/DATA/TITLFNT.NUT"
	tail -c +3769328090 "$FILE" | head -c +973014 \
		> "$SCRIPTID/DATA/TOMINE.SAN"
	tail -c +3770301104 "$FILE" | head -c +2924 \
		> "$SCRIPTID/DATA/TORANCH.FLU"
	tail -c +3770304028 "$FILE" | head -c +35275322 \
		> "$SCRIPTID/DATA/TORANCH.SAN"
	tail -c +3805579350 "$FILE" | head -c +1724 \
		> "$SCRIPTID/DATA/TOVISTA1.FLU"
	tail -c +3805581074 "$FILE" | head -c +15192562 \
		> "$SCRIPTID/DATA/TOVISTA1.SAN"
	tail -c +3820773636 "$FILE" | head -c +1964 \
		> "$SCRIPTID/DATA/TOVISTA2.FLU"
	tail -c +3820775600 "$FILE" | head -c +19334062 \
		> "$SCRIPTID/DATA/TOVISTA2.SAN"
	tail -c +3840109662 "$FILE" | head -c +520446 \
		> "$SCRIPTID/DATA/VISTTHRU.SAN"
	tail -c +3841200496 "$FILE" | head -c +627976 \
		> "$SCRIPTID/DATA/WR2_BENC.SAN"
	tail -c +3841828472 "$FILE" | head -c +644674 \
		> "$SCRIPTID/DATA/WR2_BENR.SAN"
	tail -c +3840630108 "$FILE" | head -c +570388 \
		> "$SCRIPTID/DATA/WR2_BEN.SAN"
	tail -c +3842473146 "$FILE" | head -c +645828 \
		> "$SCRIPTID/DATA/WR2_BENV.SAN"
	tail -c +3843118974 "$FILE" | head -c +1357650 \
		> "$SCRIPTID/DATA/WR2_CAVE.SAN"
	tail -c +3844476624 "$FILE" | head -c +478234 \
		> "$SCRIPTID/DATA/WR2_CVKO.SAN"
	tail -c +3844954858 "$FILE" | head -c +496876 \
		> "$SCRIPTID/DATA/WR2_ROTT.SAN"
	tail -c +3845451734 "$FILE" | head -c +450018 \
		> "$SCRIPTID/DATA/WR2_VLTC.SAN"
	tail -c +3845901752 "$FILE" | head -c +460836 \
		> "$SCRIPTID/DATA/WR2_VLTP.SAN"
	tail -c +3846362588 "$FILE" | head -c +443204 \
		> "$SCRIPTID/DATA/WR2_VLTS.SAN"
	tail -c +4200617804 "$FILE" | head -c +1152242 \
		> "$SCRIPTID/VIDEO/2009_10.SAN"
	tail -c +3847050127 "$FILE" | head -c +342 \
		> "$SCRIPTID/VIDEO/2009_10.TRS"
	tail -c +4201770046 "$FILE" | head -c +582372 \
		> "$SCRIPTID/VIDEO/ACCIDENT.SAN"
	tail -c +3847050469 "$FILE" | head -c +197 \
		> "$SCRIPTID/VIDEO/ACCIDENT.TRS"
	tail -c +4202352418 "$FILE" | head -c +478940 \
		> "$SCRIPTID/VIDEO/BARKDOG.SAN"
	tail -c +4202831358 "$FILE" | head -c +4273876 \
		> "$SCRIPTID/VIDEO/BC.SAN"
	tail -c +4207105234 "$FILE" | head -c +2283868 \
		> "$SCRIPTID/VIDEO/BENBURN.SAN"
	tail -c +4209389102 "$FILE" | head -c +3389118 \
		> "$SCRIPTID/VIDEO/BENESCPE.SAN"
	tail -c +4212778220 "$FILE" | head -c +1700582 \
		> "$SCRIPTID/VIDEO/BENHANG.SAN"
	tail -c +3847050666 "$FILE" | head -c +729 \
		> "$SCRIPTID/VIDEO/BENHANG.TRS"
	tail -c +4214478802 "$FILE" | head -c +584866 \
		> "$SCRIPTID/VIDEO/BIGBLOW.SAN"
	tail -c +4215063668 "$FILE" | head -c +2707494 \
		> "$SCRIPTID/VIDEO/BIKESWAR.SAN"
	tail -c +4217808066 "$FILE" | head -c +38266 \
		> "$SCRIPTID/VIDEO/BLINK2.SAN"
	tail -c +4217846332 "$FILE" | head -c +41946 \
		> "$SCRIPTID/VIDEO/BLINK3.SAN"
	tail -c +4217771162 "$FILE" | head -c +36904 \
		> "$SCRIPTID/VIDEO/BLINK.SAN"
	tail -c +4217888278 "$FILE" | head -c +7756700 \
		> "$SCRIPTID/VIDEO/BOLUSKIL.SAN"
	tail -c +3847051395 "$FILE" | head -c +810 \
		> "$SCRIPTID/VIDEO/BOLUSKIL.TRS"
	tail -c +4228163314 "$FILE" | head -c +792686 \
		> "$SCRIPTID/VIDEO/BURSTDOG.SAN"
	tail -c +4225644978 "$FILE" | head -c +2518336 \
		> "$SCRIPTID/VIDEO/BURST.SAN"
	tail -c +4228956000 "$FILE" | head -c +146762 \
		> "$SCRIPTID/VIDEO/CABSTRUG.SAN"
	tail -c +4229102762 "$FILE" | head -c +4426602 \
		> "$SCRIPTID/VIDEO/CATCHUP.SAN"
	tail -c +4233529364 "$FILE" | head -c +1760780 \
		> "$SCRIPTID/VIDEO/CES_NITE.SAN"
	tail -c +4235290144 "$FILE" | head -c +19553990 \
		> "$SCRIPTID/VIDEO/CREDITS.SAN"
	tail -c +3847052205 "$FILE" | head -c +3409 \
		> "$SCRIPTID/VIDEO/CREDITS.TRS"
	tail -c +4254844134 "$FILE" | head -c +436476 \
		> "$SCRIPTID/VIDEO/CRUSHDOG.SAN"
	tail -c +4255280610 "$FILE" | head -c +583454 \
		> "$SCRIPTID/VIDEO/CVRAMP.SAN"
	tail -c +4255864064 "$FILE" | head -c +5426098 \
		> "$SCRIPTID/VIDEO/DAZED.SAN"
	tail -c +3847055614 "$FILE" | head -c +1520 \
		> "$SCRIPTID/VIDEO/DAZED.TRS"
	tail -c +3847057134 "$FILE" | head -c +476 \
		> "$SCRIPTID/VIDEO/DEMOGOGG.TRS"
	tail -c +4261290162 "$FILE" | head -c +219280 \
		> "$SCRIPTID/VIDEO/DRIVEBY.SAN"
	tail -c +4261509442 "$FILE" | head -c +1197542 \
		> "$SCRIPTID/VIDEO/ESCPPLNE.SAN"
	tail -c +3847057610 "$FILE" | head -c +124 \
		> "$SCRIPTID/VIDEO/ESCPPLNE.TRS"
	tail -c +4262706984 "$FILE" | head -c +325136 \
		> "$SCRIPTID/VIDEO/FANSTOP.SAN"
	tail -c +4263454826 "$FILE" | head -c +1602904 \
		> "$SCRIPTID/VIDEO/FI_14_15.SAN"
	tail -c +3847058032 "$FILE" | head -c +729 \
		> "$SCRIPTID/VIDEO/FI_14_15.TRS"
	tail -c +4263032120 "$FILE" | head -c +422706 \
		> "$SCRIPTID/VIDEO/FI_14.SAN"
	tail -c +4265057730 "$FILE" | head -c +210464 \
		> "$SCRIPTID/VIDEO/FIRE.SAN"
	tail -c +3847057734 "$FILE" | head -c +298 \
		> "$SCRIPTID/VIDEO/FI.TRS"
	tail -c +4265268194 "$FILE" | head -c +83742 \
		> "$SCRIPTID/VIDEO/FLINCH.SAN"
	tail -c +4265351936 "$FILE" | head -c +1072450 \
		> "$SCRIPTID/VIDEO/FULPLANE.SAN"
	tail -c +4266424386 "$FILE" | head -c +291682 \
		> "$SCRIPTID/VIDEO/GOTCHA.SAN"
	tail -c +3847058761 "$FILE" | head -c +189 \
		> "$SCRIPTID/VIDEO/GOTCHA.TRS"
	tail -c +4266716068 "$FILE" | head -c +2764640 \
		> "$SCRIPTID/VIDEO/GR_LOS.SAN"
	tail -c +4269480708 "$FILE" | head -c +2173358 \
		> "$SCRIPTID/VIDEO/GR_WIN.SAN"
	tail -c +4271654066 "$FILE" | head -c +34454 \
		> "$SCRIPTID/VIDEO/HI_SIGN.SAN"
	tail -c +4271688520 "$FILE" | head -c +114586 \
		> "$SCRIPTID/VIDEO/INTO_FAN.SAN"
	tail -c +4271803106 "$FILE" | head -c +119202 \
		> "$SCRIPTID/VIDEO/INTRO4.SAN"
	tail -c +4271922308 "$FILE" | head -c +13286740 \
		> "$SCRIPTID/VIDEO/INTROD_8.SAN"
	tail -c +3847058950 "$FILE" | head -c +2318 \
		> "$SCRIPTID/VIDEO/INTROD_8.TRS"
	tail -c +4285209048 "$FILE" | head -c +4320290 \
		> "$SCRIPTID/VIDEO/JG.SAN"
	tail -c +4289529338 "$FILE" | head -c +4324612 \
		> "$SCRIPTID/VIDEO/JUMPGORG.SAN"
	tail -c +4293853950 "$FILE" | head -c +95862 \
		> "$SCRIPTID/VIDEO/KICK_OFF.SAN"
	tail -c +4293949812 "$FILE" | head -c +525504 \
		> "$SCRIPTID/VIDEO/KILLGULL.SAN"
	tail -c +4294475316 "$FILE" | head -c +1720434 \
		> "$SCRIPTID/VIDEO/KR.SAN"
	tail -c +4299655452 "$FILE" | head -c +6560066 \
		> "$SCRIPTID/VIDEO/KS_111.SAN"
	tail -c +3847061979 "$FILE" | head -c +1892 \
		> "$SCRIPTID/VIDEO/KS_111.TRS"
	tail -c +4299402634 "$FILE" | head -c +252818 \
		> "$SCRIPTID/VIDEO/KS_11.SAN"
	tail -c +3847061904 "$FILE" | head -c +75 \
		> "$SCRIPTID/VIDEO/KS_11.TRS"
	tail -c +4296195750 "$FILE" | head -c +3206884 \
		> "$SCRIPTID/VIDEO/KS_1.SAN"
	tail -c +3847061268 "$FILE" | head -c +636 \
		> "$SCRIPTID/VIDEO/KS_1.TRS"
	tail -c +4306215518 "$FILE" | head -c +606522 \
		> "$SCRIPTID/VIDEO/KS_IV.SAN"
	tail -c +3847063871 "$FILE" | head -c +279 \
		> "$SCRIPTID/VIDEO/KS_IV.TRS"
	tail -c +4306822040 "$FILE" | head -c +9394014 \
		> "$SCRIPTID/VIDEO/KS_V.SAN"
	tail -c +3847064150 "$FILE" | head -c +2437 \
		> "$SCRIPTID/VIDEO/KS_V.TRS"
	tail -c +4316216054 "$FILE" | head -c +907382 \
		> "$SCRIPTID/VIDEO/KS_X.SAN"
	tail -c +4317123436 "$FILE" | head -c +472730 \
		> "$SCRIPTID/VIDEO/LIMOBY.SAN"
	tail -c +4317596166 "$FILE" | head -c +443248 \
		> "$SCRIPTID/VIDEO/LIMOROAR.SAN"
	tail -c +3847066587 "$FILE" | head -c +189 \
		> "$SCRIPTID/VIDEO/MIRANDA.TRS"
	tail -c +4318128922 "$FILE" | head -c +979820 \
		> "$SCRIPTID/VIDEO/MOEJECT.SAN"
	tail -c +3847066776 "$FILE" | head -c +199 \
		> "$SCRIPTID/VIDEO/MOEJECT.TRS"
	tail -c +4318039414 "$FILE" | head -c +89508 \
		> "$SCRIPTID/VIDEO/MO_FUME.SAN"
	tail -c +4319108742 "$FILE" | head -c +195520 \
		> "$SCRIPTID/VIDEO/MOREACH.SAN"
	tail -c +4319304262 "$FILE" | head -c +3562268 \
		> "$SCRIPTID/VIDEO/MWC.SAN"
	tail -c +3847066975 "$FILE" | head -c +196 \
		> "$SCRIPTID/VIDEO/MWC.TRS"
	tail -c +4322866530 "$FILE" | head -c +509122 \
		> "$SCRIPTID/VIDEO/NAMES_MO.SAN"
	tail -c +3847067171 "$FILE" | head -c +186 \
		> "$SCRIPTID/VIDEO/NAMES_MO.TRS"
	tail -c +4323375652 "$FILE" | head -c +149892 \
		> "$SCRIPTID/VIDEO/NB_BLOW.SAN"
	tail -c +4323525544 "$FILE" | head -c +2876354 \
		> "$SCRIPTID/VIDEO/NITEFADE.SAN"
	tail -c +4326401898 "$FILE" | head -c +799826 \
		> "$SCRIPTID/VIDEO/NITERIDE.SAN"
	tail -c +4327201724 "$FILE" | head -c +70752 \
		> "$SCRIPTID/VIDEO/NTREACT.SAN"
	tail -c +4327272476 "$FILE" | head -c +74568 \
		> "$SCRIPTID/VIDEO/OFFGOGG.SAN"
	tail -c +4327347044 "$FILE" | head -c +20639282 \
		> "$SCRIPTID/VIDEO/OM.SAN"
	tail -c +3847067357 "$FILE" | head -c +1294 \
		> "$SCRIPTID/VIDEO/OM.TRS"
	tail -c +4347986326 "$FILE" | head -c +537706 \
		> "$SCRIPTID/VIDEO/PLNCRSH.SAN"
	tail -c +3847068651 "$FILE" | head -c +151 \
		> "$SCRIPTID/VIDEO/POLEEXIT.TRS"
	tail -c +4348524032 "$FILE" | head -c +1105120 \
		> "$SCRIPTID/VIDEO/REACHHER.SAN"
	tail -c +4349629152 "$FILE" | head -c +3924328 \
		> "$SCRIPTID/VIDEO/REALRIDE.SAN"
	tail -c +3847068802 "$FILE" | head -c +235 \
		> "$SCRIPTID/VIDEO/REALRIDE.TRS"
	tail -c +3847069037 "$FILE" | head -c +480 \
		> "$SCRIPTID/VIDEO/RESTSTOP.TRS"
	tail -c +4353553480 "$FILE" | head -c +53042 \
		> "$SCRIPTID/VIDEO/REV.SAN"
	tail -c +4353606522 "$FILE" | head -c +67980 \
		> "$SCRIPTID/VIDEO/RIDEAWAY.SAN"
	tail -c +4353674502 "$FILE" | head -c +3924328 \
		> "$SCRIPTID/VIDEO/RIDEOUT.SAN"
	tail -c +4358366270 "$FILE" | head -c +269632 \
		> "$SCRIPTID/VIDEO/RIPBROW.SAN"
	tail -c +3847069581 "$FILE" | head -c +480 \
		> "$SCRIPTID/VIDEO/RIPBROW.TRS"
	tail -c +3847070061 "$FILE" | head -c +228 \
		> "$SCRIPTID/VIDEO/RIPFELL.TRS"
	tail -c +4357598830 "$FILE" | head -c +767440 \
		> "$SCRIPTID/VIDEO/RIP_KILL.SAN"
	tail -c +3847069517 "$FILE" | head -c +64 \
		> "$SCRIPTID/VIDEO/RIP_KILL.TRS"
	tail -c +4358635902 "$FILE" | head -c +321780 \
		> "$SCRIPTID/VIDEO/RIPSHIFT.SAN"
	tail -c +4358957682 "$FILE" | head -c +1366958 \
		> "$SCRIPTID/VIDEO/SCRAPBOT.SAN"
	tail -c +3847070289 "$FILE" | head -c +96 \
		> "$SCRIPTID/VIDEO/SCRAPBOT.TRS"
	tail -c +4360324640 "$FILE" | head -c +196140 \
		> "$SCRIPTID/VIDEO/SEEN_RIP.SAN"
	tail -c +3847070385 "$FILE" | head -c +480 \
		> "$SCRIPTID/VIDEO/SEEN_RIP.TRS"
	tail -c +4360520780 "$FILE" | head -c +681878 \
		> "$SCRIPTID/VIDEO/SHEFALLS.SAN"
	tail -c +3847070865 "$FILE" | head -c +729 \
		> "$SCRIPTID/VIDEO/SHEFALLS.TRS"
	tail -c +4361202658 "$FILE" | head -c +996658 \
		> "$SCRIPTID/VIDEO/SHORTOPN.SAN"
	tail -c +3847071594 "$FILE" | head -c +630 \
		> "$SCRIPTID/VIDEO/SHOWDOWN.TRS"
	tail -c +4362199316 "$FILE" | head -c +54038 \
		> "$SCRIPTID/VIDEO/SHOWSMRK.SAN"
	tail -c +4362253354 "$FILE" | head -c +1129920 \
		> "$SCRIPTID/VIDEO/SHOWUP1.SAN"
	tail -c +3847072224 "$FILE" | head -c +729 \
		> "$SCRIPTID/VIDEO/SHOWUP1.TRS"
	tail -c +4363383274 "$FILE" | head -c +543854 \
		> "$SCRIPTID/VIDEO/SNAPFOTO.SAN"
	tail -c +3847072953 "$FILE" | head -c +189 \
		> "$SCRIPTID/VIDEO/SNAPFOTO.TRS"
	tail -c +4363927128 "$FILE" | head -c +1857478 \
		> "$SCRIPTID/VIDEO/SNSETRDE.SAN"
	tail -c +3847073142 "$FILE" | head -c +729 \
		> "$SCRIPTID/VIDEO/SNSETRDE.TRS"
	tail -c +4365784606 "$FILE" | head -c +65822 \
		> "$SCRIPTID/VIDEO/SQUINT.SAN"
	tail -c +4365850428 "$FILE" | head -c +3101852 \
		> "$SCRIPTID/VIDEO/TC.SAN"
	tail -c +4368952280 "$FILE" | head -c +2279844 \
		> "$SCRIPTID/VIDEO/TEETBURN.SAN"
	tail -c +4371232124 "$FILE" | head -c +234726 \
		> "$SCRIPTID/VIDEO/TINYPLNE.SAN"
	tail -c +4371466850 "$FILE" | head -c +126268 \
		> "$SCRIPTID/VIDEO/TINYTRUC.SAN"
	tail -c +4371593118 "$FILE" | head -c +186312 \
		> "$SCRIPTID/VIDEO/TINYTRUK.SAN"
	tail -c +4371779430 "$FILE" | head -c +1769204 \
		> "$SCRIPTID/VIDEO/TOKCRGO.SAN"
	tail -c +3847073871 "$FILE" | head -c +607 \
		> "$SCRIPTID/VIDEO/TOKCRGO.TRS"
	tail -c +4373548634 "$FILE" | head -c +1202448 \
		> "$SCRIPTID/VIDEO/TRKCRSH.SAN"
	tail -c +4374751082 "$FILE" | head -c +968938 \
		> "$SCRIPTID/VIDEO/VISION.SAN"
	tail -c +4375720020 "$FILE" | head -c +128612 \
		> "$SCRIPTID/VIDEO/WEREINIT.SAN"
	tail -c +3847074478 "$FILE" | head -c +480 \
		> "$SCRIPTID/VIDEO/WEREINIT.TRS"
	tail -c +4375848632 "$FILE" | head -c +744156 \
		> "$SCRIPTID/VIDEO/WHIP_PAN.SAN"
	tail -c +3847074958 "$FILE" | head -c +194 \
		> "$SCRIPTID/VIDEO/WHIP_PAN.TRS"

	echo "Script $SCRIPTID.sh done"
}
