#!/bin/sh

# Extraction script for:
# Pokemon Box - Ruby & Sapphire (Europe) (En,Fr,De,Es,It).iso

# Outputs:
# - Pokemon - Ruby Version (USA, Europe) (Rev 1).gba
# - Pokemon - Ruby Version (USA, Europe) (Rev 2).gba
# - Pokemon - Rubin-Edition (Germany).gba
# - Pokemon - Rubin-Edition (Germany) (Rev 1).gba
# - Pokemon - Version Rubis (France).gba
# - Pokemon - Version Rubis (France) (Rev 1).gba
# - Pokemon - Versione Rubino (Italy).gba
# - Pokemon - Versione Rubino (Italy) (Rev 1).gba
# - Pokemon - Edicion Rubi (Spain).gba
# - Pokemon - Edicion Rubi (Spain) (Rev 1).gba
# - Pokemon - Sapphire Version (USA, Europe) (Rev 1).gba
# - Pokemon - Sapphire Version (USA, Europe) (Rev 2).gba
# - Pokemon - Saphir-Edition (Germany).gba
# - Pokemon - Saphir-Edition (Germany) (Rev 1).gba
# - Pokemon - Version Saphir (France).gba
# - Pokemon - Version Saphir (France) (Rev 1).gba
# - Pokemon - Versione Zaffiro (Italy).gba
# - Pokemon - Versione Zaffiro (Italy) (Rev 1).gba
# - Pokemon - Edicion Zafiro (Spain).gba
# - Pokemon - Edicion Zafiro (Spain) (Rev 1).gba
# - [BIOS] Game Boy Advance (World).gba

# Requires: wit

romextract()
{
	dependency_wit          || return 1

	echo "Extracting files from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" --files=+/files/*.srl --files=+/sys/main.dol

	echo "Moving ROMs ..."
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPD00.srl" \
		"$SCRIPTID/Pokemon - Saphir-Edition (Germany).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPD10.srl" \
		"$SCRIPTID/Pokemon - Saphir-Edition (Germany) (Rev 1).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPE01.srl" \
		"$SCRIPTID/Pokemon - Sapphire Version (USA, Europe) (Rev 1).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPE20.srl" \
		"$SCRIPTID/Pokemon - Sapphire Version (USA, Europe) (Rev 2).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPF00.srl" \
		"$SCRIPTID/Pokemon - Version Saphir (France).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPF10.srl" \
		"$SCRIPTID/Pokemon - Version Saphir (France) (Rev 1).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPI00.srl" \
		"$SCRIPTID/Pokemon - Versione Zaffiro (Italy).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPI10.srl" \
		"$SCRIPTID/Pokemon - Versione Zaffiro (Italy) (Rev 1).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPS00.srl" \
		"$SCRIPTID/Pokemon - Edicion Zafiro (Spain).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXPS10.srl" \
		"$SCRIPTID/Pokemon - Edicion Zafiro (Spain) (Rev 1).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVD00.srl" \
		"$SCRIPTID/Pokemon - Rubin-Edition (Germany).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVD10.srl" \
		"$SCRIPTID/Pokemon - Rubin-Edition (Germany) (Rev 1).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVE01.srl" \
		"$SCRIPTID/Pokemon - Ruby Version (USA, Europe) (Rev 1).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVE20.srl" \
		"$SCRIPTID/Pokemon - Ruby Version (USA, Europe) (Rev 2).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVF00.srl" \
		"$SCRIPTID/Pokemon - Version Rubis (France).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVF10.srl" \
		"$SCRIPTID/Pokemon - Version Rubis (France) (Rev 1).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVI00.srl" \
		"$SCRIPTID/Pokemon - Versione Rubino (Italy).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVI10.srl" \
		"$SCRIPTID/Pokemon - Versione Rubino (Italy) (Rev 1).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVS00.srl" \
		"$SCRIPTID/Pokemon - Edicion Rubi (Spain).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/files/AAXVS10.srl" \
		"$SCRIPTID/Pokemon - Edicion Rubi (Spain) (Rev 1).gba"

	echo "Extractinf BIOS ..."
	tail -c +1551913 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GPXP/sys/main.dol" | 
		head -c +16384 > "$SCRIPTID/[BIOS] Game Boy Advance (World).gba"

	echo "Patching BIOS ..."
	printf '\004' | dd status=none of="$SCRIPTID/[BIOS] Game Boy Advance (World).gba" count=1 bs=1 seek=$((0x340)) conv=notrunc
	printf '\000' | dd status=none of="$SCRIPTID/[BIOS] Game Boy Advance (World).gba" count=1 bs=1 seek=$((0x342)) conv=notrunc
	printf '\033' | dd status=none of="$SCRIPTID/[BIOS] Game Boy Advance (World).gba" count=1 bs=1 seek=$((0x343)) conv=notrunc
	printf '\002' | dd status=none of="$SCRIPTID/[BIOS] Game Boy Advance (World).gba" count=1 bs=1 seek=$((0x348)) conv=notrunc
	printf '\000' | dd status=none of="$SCRIPTID/[BIOS] Game Boy Advance (World).gba" count=1 bs=1 seek=$((0x34a)) conv=notrunc
	printf '\353' | dd status=none of="$SCRIPTID/[BIOS] Game Boy Advance (World).gba" count=1 bs=1 seek=$((0x34b)) conv=notrunc

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
