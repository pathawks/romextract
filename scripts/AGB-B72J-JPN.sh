#!/bin/sh

# Extraction script for:
# Hudson Best Collection Vol. 2 - Lode Runner Collection (Japan).gba

# Outputs:
# - Lode Runner (Japan) (Hudson Best Collection) (Unverified).nes
# - Championship Lode Runner (Japan) (Hudson Best Collection) (Unverified).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +189557 "$FILE" | head -c +24592 \
		> "$SCRIPTID/Lode Runner (Japan) (Hudson Best Collection) (Unverified).nes"
	tail -c +214149 "$FILE" | head -c +24592 \
		> "$SCRIPTID/Championship Lode Runner (Japan) (Hudson Best Collection) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
