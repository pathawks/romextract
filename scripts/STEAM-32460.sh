#!/bin/sh

# Extraction script for:
# Monkey Island 2 Special Edition: LeChuck’s Revenge (Monkey2.pak)

# Outputs:
# - Monkey Island 2: LeChuck's Revenge (DOS/English)

romextract()
{
	if [ -f "$(dirname "$FILE")/monkey2.pak" ]; then
		FILE="$(dirname "$FILE")/monkey2.pak"
	else
		echo "Could not find monkey2.pak"
		return 1
	fi

	echo "Extracting game files from monkey2.pak ..."
	# tail for offset, head for game file size
	tail -c +1391289 "$FILE" | head -c +11135 \
		> "$SCRIPTID/monkey2.000"
	tail -c +1402424 "$FILE" | head -c +9080329 \
		> "$SCRIPTID/monkey2.001"

	echo "Script $SCRIPTID.sh done"
}
