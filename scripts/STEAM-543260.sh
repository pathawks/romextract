#!/bin/sh

# Extraction script for:
# Wonder Boy: The Dragon's Trap

# Outputs:
# - Wonder Boy III - The Dragon's Trap (USA, Europe) (Unl) (Unverified).sms

romextract()
{
	
	echo "Copying uncompressed ROM ..."
	cp "$(dirname "$FILE")/../bin_pc/rom/wb3.sms" "$SCRIPTID/Wonder Boy III - The Dragon's Trap (USA, Europe) (Unl) (Unverified).sms"

	echo "Fixing ROM header ..."
	printf 'TMR SEGA' | dd status=none of="$SCRIPTID/Wonder Boy III - The Dragon's Trap (USA, Europe) (Unl) (Unverified).sms" count=8 bs=1 seek=$((0x7ff0)) conv=notrunc

	echo "Script $SCRIPTID.sh done"
}
